import axios from 'axios';

const ax = axios.create({
  baseURL: 'https://cloud.iexapis.com/stable',
});

ax.interceptors.request.use(req => {
  if (req.method === 'get') {
    req.params = req.params || {};
    req.params['token'] = process.env.REACT_APP_IEX_API_SECRET;
  }
  return req;
});

export default ax;
