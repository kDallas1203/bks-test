import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import './app.scss';
import { Layout } from 'antd';
import MainPage from '../../pages/main-page';
import { initBonds } from '../../store/portfolio/actions';
const { Content, Header } = Layout;

function AppLayout(props) {
  const { dispatch } = props;
  useEffect(() => {
    dispatch(initBonds());
  }, []);
  return (
    <Layout>
      <Header>
        <div style={{ color: '#FFF', fontSize: '24px' }}>Watcher</div>
      </Header>
      <Content style={{ padding: '0 50px', marginTop: 64 }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
          <MainPage />
        </div>
      </Content>
    </Layout>
  );
}

export default connect(({ symbols }) => ({ symbols }))(AppLayout);
