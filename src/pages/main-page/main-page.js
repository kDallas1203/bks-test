import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addToFavoritePortfolio, deleteBond } from '../../store/portfolio/actions';
import _ from 'lodash';
import { Drawer, Card, Icon, Statistic, Button, message } from 'antd';
import SearchPage from '../../components/search-bond';

import './main-page.scss';

const MainPage = props => {
  const { dispatch } = props;
  const { payload: portfolio, isFetching } = props.portfolio;
  const [drawerStatus, setDrawerStatus] = useState(false);
  if (isFetching) {
    return (
      <div style={{ textAlign: 'center' }}>
        <Icon type="loading" style={{ fontSize: 48 }} spin />
      </div>
    );
  }

  const onDelete = e => {
    dispatch(deleteBond(e.target.dataset.symbol));
  };

  const addBound = async bound => {
    await dispatch(addToFavoritePortfolio(bound));
    message.success('Бумага добавлена в избранное');
    setDrawerStatus(false);
  };

  return (
    <>
      <div>
        <Button type="primary" size="large" icon="search" onClick={() => setDrawerStatus(true)}>
          Поиск бумаг
        </Button>
        <div className="main-page__bounds">
          {!_.isEmpty(portfolio) &&
            portfolio.map((bound) => (
              <div className="main-page__card">
                <Card bordered style={{ width: 300 }}>
                  <Button
                    data-symbol={bound.symbol}
                    shape="circle"
                    icon="close"
                    size="small"
                    className="main-page__card__button"
                    type="default"
                    onClick={onDelete}
                  />
                  <Statistic title={bound.companyName} value={bound.latestPrice} />
                </Card>
              </div>
            ))}
        </div>
      </div>
      <Drawer
        title="Поиск бумаг"
        width={500}
        onClose={() => setDrawerStatus(false)}
        visible={drawerStatus}
        placement="left">
        <SearchPage onSelect={bound => addBound(bound)} />
      </Drawer>
    </>
  );
};

export default connect(({ portfolio }) => ({ portfolio }))(MainPage);
