import request from './requester';
export const apiGetSymbols = async () => {
  try {
    return await request.get('/ref-data/iex/symbols');
  } catch (e) {
    console.error('API get symbols error: ', e);
    return new Error(e);
  }
};

export const apiGetBond = async symbol => {
  try {
    return await request.get(`/stock/${symbol}/quote`);
  } catch (e) {
    console.error('API get bound error: ', e);
    return new Error(e);
  }
};
