import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Icon, Input, Result, Typography } from 'antd';
import _ from 'lodash';
import { apiGetBond } from '../../api';
import { getSymbols } from '../../store/symbols/actions';
import './search-bond.scss';

function SearchPageModal(props) {
  const { dispatch } = props;
  const { payload: portfolio } = props.portfolio;
  const [selectedBond, setSelectedBond] = useState({});
  const [bondFetching, setBondFetching] = useState(false);
  const { payload: symbols, isFetching } = props.symbols;

  useEffect(() => {
    dispatch(getSymbols());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSearch = async value => {
    const findSymbol = symbols.find(({ symbol }) => {
      return symbol.toUpperCase() === value.toUpperCase();
    });
    if (!findSymbol) {
      setSelectedBond('NOT_FOUND');
      return null;
    } else {
      setBondFetching(true);
      apiGetBond(findSymbol.symbol)
        .then(({ data }) => {
          const bondExist = !!portfolio.find(({ symbol }) => symbol === data.symbol);
          console.log('bondExist', bondExist);
          setSelectedBond({ ...data, exist: bondExist });
        })
        .finally(() => setBondFetching(false));
    }
  };

  if (isFetching) {
    return (
      <div style={{ textAlign: 'center' }}>
        <Icon type="loading" style={{ fontSize: 48 }} spin />
      </div>
    );
  }

  return (
    <div>
      <div className="search-page__head">
        <Input.Search
          className="head__input"
          placeholder="Поиск бумаги"
          loading={bondFetching}
          size="large"
          disabled={bondFetching}
          onSearch={handleSearch}
        />
      </div>
      <div className="search-page__content">
        {selectedBond === 'NOT_FOUND' && <Result status="404" title="Ничего не найдено" />}
        {!_.isEmpty(selectedBond) && typeof selectedBond === 'object' && (
          <div>
            <Typography>Результаты поиска</Typography>
            <div
              className={`search-page item ${selectedBond.exist && 'item_exist'}`}
              onClick={() => !selectedBond.exist && props.onSelect(selectedBond)}>
              <Typography.Title level={4}>
                {selectedBond.companyName} {selectedBond.exist ? <Icon type="check" /> : <Icon type="star" />}
              </Typography.Title>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default connect(({ symbols, portfolio }) => ({ symbols, portfolio }))(SearchPageModal);
