import React from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import { Menu } from 'antd';

function NavLinkMenu(props) {
  const { location } = props;
  return (
    <Menu theme="dark" mode="horizontal" selectedKeys={[location.pathname]} style={{ lineHeight: '64px' }}>
      {props.routes.map(route => (
        <Menu.Item key={route.to}>
          <NavLink to={route.to}>{route.name}</NavLink>
        </Menu.Item>
      ))}
    </Menu>
  );
}

export default withRouter(NavLinkMenu);
