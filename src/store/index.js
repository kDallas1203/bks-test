import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

// reducers
import symbols from './symbols/reducers';
import portfolio from './portfolio/reducers';

const rootReducer = combineReducers({
  symbols,
  portfolio,
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
export default store;
