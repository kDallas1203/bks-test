export const FETCH_SYMBOLS = 'FETCH_SYMBOLS';
export const SET_SYMBOLS = 'SET_SYMBOLS';
export const ERROR_SYMBOLS = 'ERROR_SYMBOLS';

const initialState = {
  payload: [],
  isFetching: false,
  isError: false,
  err: '',
};

const symbolsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SYMBOLS: {
      return { ...state, isFetching: action.isFetching };
    }
    case SET_SYMBOLS: {
      return { ...state, payload: action.payload };
    }
    case ERROR_SYMBOLS: {
      return { ...state, isError: true, err: action.err };
    }
    default:
      return state;
  }
};

export default symbolsReducer;
