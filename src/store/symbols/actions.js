import { apiGetSymbols } from '../../api';

import { FETCH_SYMBOLS, SET_SYMBOLS, ERROR_SYMBOLS } from './reducers';

export const symbols_fetch = status => {
  return {
    type: FETCH_SYMBOLS,
    isFetching: status,
  };
};

export const symbols_set = symbols => {
  return {
    type: SET_SYMBOLS,
    payload: symbols,
  };
};

export const error_symbols = error => {
  return {
    type: ERROR_SYMBOLS,
    err: error,
  };
};

export const getSymbols = () => async dispatch => {
  try {
    dispatch(symbols_fetch(true));
    const symbols = await apiGetSymbols();
    dispatch(symbols_set(symbols.data));
    dispatch(symbols_fetch(false));
  } catch (e) {
    dispatch(error_symbols(e));
  }
};
