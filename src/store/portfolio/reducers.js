export const FETCH_PORTFOLIO = 'FETCH_PORTFOLIOS';
export const SET_PORTFOLIO = 'SET_PORTFOLIO';
export const ERROR_PORTFOLIO = 'ERROR_PORTFOLIO';
export const ADD_FAVORITE = 'ADD_FAVORITE';
export const DELETE_BOND = 'DELETE_BOND';

const initialState = {
  payload: [],
  isFetching: false,
  isError: false,
  err: '',
};

const portfolioReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FAVORITE:
      return { ...state, payload: [...state.payload, action.bound] };
    case SET_PORTFOLIO:
      return { ...state, payload: action.bonds };
    case FETCH_PORTFOLIO:
      return {...state, isFetching: action.isFetching}
    case DELETE_BOND:
      const itemIdx = state.payload.findIndex(({symbol}) => symbol === action.symbol)
      return {
        ...state,
        payload: [
          ...state.payload.slice(0, itemIdx),
          ...state.payload.slice(itemIdx + 1)
        ]
      }
    default:
      return state;
  }
};

export default portfolioReducer;
