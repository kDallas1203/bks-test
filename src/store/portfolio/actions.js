import { ADD_FAVORITE, FETCH_PORTFOLIO, SET_PORTFOLIO, ERROR_PORTFOLIO, DELETE_BOND } from './reducers';
import { apiGetBond } from '../../api';

export const portfolio_add_favorite = bound => {
  return {
    type: ADD_FAVORITE,
    bound,
  };
};

export const portfolio_fetch = status => {
  return {
    type: FETCH_PORTFOLIO,
    isFetching: status,
  };
};

export const portfolio_set = bonds => {
  return {
    type: SET_PORTFOLIO,
    bonds,
  };
};

export const portfolio_error = err => {
  return {
    type: ERROR_PORTFOLIO,
    err,
  };
};

export const portfolio_delete = symbol => {
  return {
    type: DELETE_BOND,
    symbol,
  };
};

export const initBonds = () => dispatch => {
  const serializeStorage = localStorage.getItem('bonds');
  if (serializeStorage !== null) {
    const bonds = JSON.parse(serializeStorage);
    dispatch(portfolio_fetch(true));
    Promise.all(
      bonds.map(bond => {
        try {
          return apiGetBond(bond.symbol).then(({ data }) => data);
        } catch (e) {
          portfolio_error(e);
        }
      })
    )
      .then(data => dispatch(portfolio_set(data)))
      .finally(() => dispatch(portfolio_fetch(false)));
  }
};

export const addToFavoritePortfolio = bond => async dispatch => {
  const serializeStorage = localStorage.getItem('bonds');
  if (serializeStorage === null) {
    localStorage.setItem('bonds', JSON.stringify([bond]));
  } else {
    const localBonds = JSON.parse(serializeStorage);
    localStorage.setItem('bonds', JSON.stringify([...localBonds, bond]));
  }
  return dispatch(portfolio_add_favorite(bond));
};

export const deleteBond = symbol => async dispatch => {
  try {
    const bonds = JSON.parse(localStorage.getItem('bonds'));
    const itemIdx = bonds.findIndex(bond => bond.symbol === symbol);
    localStorage.setItem('bonds', JSON.stringify([...bonds.slice(0, itemIdx), ...bonds.slice(itemIdx + 1)]));
    dispatch(portfolio_delete(symbol));
  } catch (e) {
    dispatch(portfolio_error(e));
  }
};
